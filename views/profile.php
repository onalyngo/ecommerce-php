<?php
	require "../partials/template.php";

	function get_title(){
		echo "Profile";
	}

	function get_body_contents(){
		require "../controllers/connection.php";

?>

	<h1 class="text-center py-5">Profile Page</h1>

	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h3>Profile Information:</h3>
				<?php
					$userId = $_SESSION['user']['id'];
					$user_query = "SELECT * FROM users WHERE id = '$userId'";					
					$users = mysqli_query($conn, $user_query);

					foreach($users as $indiv_user){
					?>
					<ul>
						<li>First Name: <?php echo $indiv_user['firstName']?> </li>
						<li>Last Name: <?php echo $indiv_user['lastName']?></li>
						<li>Email address: <?php echo $indiv_user['email']?></li>
					</ul>
				<h3>Addresses:</h3>
				<ul>
					<?php

					$userId = $_SESSION['user']['id'];
					$address_query = "SELECT * FROM addresses WHERE user_id = '$userId'";					
					$addresses = mysqli_query($conn, $address_query);

					foreach($addresses as $indiv_address){
					?>
					<li><?php echo $indiv_address['address1'] . ", " . $indiv_address['address2']. "<br>" . $indiv_address['city']. " " . $indiv_address['zipcode'] ?></li>
					<?php
					}
					?>
				</ul>
					<?php
					}
					?>
				<h3>Contacts:</h3>
				<ul>
					<?php

					$userId = $_SESSION['user']['id'];
					$contact_query = "SELECT * FROM contacts WHERE user_id = '$userId'";	

					$contactNo = mysqli_query($conn, $contact_query);

					foreach($contactNo as $indiv_contact){
					?>
					<li><?php echo $indiv_contact['contactNo']?></li>
					<?php
					}
					?>
				</ul>
				<!-- Sa div na to, publish niyo yung profile details -->
			</div>
			<div class="col-lg-6">
				<form action="../controllers/add-address-process.php" method="POST">
					<div class="form-group">
						<label for="address1">Address1:</label>
						<input type="text" name="address1" class="form-control">			
					</div>
					<div class="form-group">
						<label for="address2">Address2:</label>
						<input type="text" name="address2" class="form-control">			
					</div>
					<div class="form-group">
						<label for="city">City:</label>
						<input type="text" name="city" class="form-control">
					</div>
					<div class="form-group">
						<label for="zipCode">Zip Code:</label>
						<input type="text" name="zipCode" class="form-control">	
					</div>
					<input type="hidden" name="user_id" value="<?php echo $userId?>">
					<button class="btn btn-secondary" type="submit">Add Address</button>
				</form>
				<hr>
				<form action="../controllers/add-contact-process.php" method="POST">
					<div class="form-group">
						<label for="contactNo">Contact number:</label>
						<input type="text" name="contactNo" class="form-control">		
					</div>
					<input type="hidden" name="user_id" value="<?php echo $userId?>">
					<button class="btn btn-secondary" type="submit">Add Contact</button>
				</form>
			</div>
		</div>
	</div>



<?php
	}
?>