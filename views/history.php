<?php
	require "../partials/template.php";
	function get_title(){
		echo "Order History";
	}
	function get_body_contents(){
		require "../controllers/connection.php";
?>

	<h1 class="text-center py-5">History of Orders</h1>
	<div class="col-lg-8 offset-lg-2">
		<table class="table table-striped table-dark">
		  <thead>
		    <tr>
		      <th>Order ID</th>
		      <th>Item Name</th>
		      <th>Total</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php
		  	$userId = $_SESSION['user']['id'];
			$history_query = "SELECT * FROM orders WHERE user_id = '$userId'";

			$history = mysqli_query($conn, $history_query);

			foreach($history as $indiv_history){
			?>
		    <tr>
		      <td><?php echo $indiv_history['id']?></td>
		      <td>
		      	<?php
		      		$orderId = $indiv_history['id'];
		      		$items_query = "SELECT * FROM items JOIN item_order ON (items.id = item_order.item_id) WHERE item_order.order_id = $orderId";

		      		$items = mysqli_query($conn, $items_query);
		      		
		      		foreach($items as $indiv_item){
		    	?>
		    	<span><?php echo $indiv_item['name']?></span><br>		
		      		<?php
		      		}
		    		?>
		      </td>
		      <td><?php echo $indiv_order['total']?></td>
		    </tr>
		    <?php
		    }
		    ?>
		  </tbody>
		</table>
	</div>
	<?php		
	}
	?>